# README #

Three Dockerfiles are provided here. The two .local files work and the .AWS is more of a template of what we could do.
### Dockerfile.local.runner ###
Use this if you want to compile the applications on your laptop with the usual `mvn package...` commands.

This Dockerfile uses a JRE based image that is significantly smaller than JDK based image.
This Dockerfile copies the already compiled jar into the image and specifies the CMD to start the container.
### Dockerfile.local.build ###
Use this if you want to build within the source code inside Docker. 

This Dockerfile starts with a JDK based docker image to build the application. 
Then copies the resulting `jar` to a JRE based docker image that is much smaller than the JDK one.

### Dockerfile.AWS ###
This is an example of what the Dockerfile that is used in AWS might look like. 
The base image will have added certificates and tooling. We developers will not need to alter this.